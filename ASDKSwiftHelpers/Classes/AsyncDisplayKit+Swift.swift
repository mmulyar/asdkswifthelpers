//
// Created by Mikhail Mulyar on 06/11/16.
// Copyright (c) 2016 Mikhail Mulyar. All rights reserved.
//

import UIKit
import AsyncDisplayKit


//MARK: - Specs


extension ASLayoutElement {

    public func asElement() -> ASLayoutElement {
        return self
    }

    public func insets(_ insets: UIEdgeInsets) -> ASLayoutSpec {
        return ASInsetLayoutSpec(insets: insets, child: self)
    }

    public func insets(_ insets: CGFloat) -> ASLayoutSpec {
        return ASInsetLayoutSpec(insets: UIEdgeInsetsMake(insets, insets, insets, insets), child: self)
    }

    public func insets(_ top: CGFloat, _ left: CGFloat, _ bottom: CGFloat, _ right: CGFloat) -> ASLayoutSpec {
        return ASInsetLayoutSpec(insets: UIEdgeInsetsMake(top, left, bottom, right), child: self)
    }

    public func overlay(_ overlay: ASLayoutElement) -> ASLayoutSpec {
        return ASOverlayLayoutSpec(child: self, overlay: overlay)
    }

    public func background(_ background: ASLayoutElement) -> ASLayoutSpec {
        return ASBackgroundLayoutSpec(child: self, background: background)
    }

    public func wrapped() -> ASLayoutSpec {
        return ASWrapperLayoutSpec(layoutElement: self)
    }

    public func absolute() -> ASLayoutSpec {
        return ASAbsoluteLayoutSpec(children: [self])
    }

    public func centered(centering: ASCenterLayoutSpecCenteringOptions = .XY,
                         sizing: ASCenterLayoutSpecSizingOptions = []) -> ASLayoutSpec {
        return ASCenterLayoutSpec(centeringOptions: centering, sizingOptions: sizing, child: self)
    }

    public func positioned(horizontal: ASRelativeLayoutSpecPosition = .start,
                           vertical: ASRelativeLayoutSpecPosition = .start,
                           sizing: ASRelativeLayoutSpecSizingOption = []) -> ASLayoutSpec {
        return ASRelativeLayoutSpec(horizontalPosition: horizontal,
                                    verticalPosition: vertical,
                                    sizingOption: sizing,
                                    child: self)
    }

    public func ratio(_ ratio: CGFloat) -> ASLayoutSpec {
        return ASRatioLayoutSpec(ratio: ratio, child: self)
    }
}


//MARK: - ASLayoutElementStyle


extension ASLayoutElement {

    public func styled(_ modification: @escaping(ASLayoutElementStyle) -> ()) -> ASLayoutElement {
        modification(self.style)
        return self
    }
}


//MARK: - ASStackLayoutElement


extension ASLayoutElement {

    public func spacingBefore(_ value: CGFloat) -> ASLayoutElement {
        self.style.spacingBefore = value
        return self
    }

    public func spacingAfter(_ value: CGFloat) -> ASLayoutElement {
        self.style.spacingAfter = value
        return self
    }

    public func flexGrow(_ value: CGFloat = 1.0) -> ASLayoutElement {
        self.style.flexGrow = value
        return self
    }

    public func flexShrink(_ value: CGFloat = 1.0) -> ASLayoutElement {
        self.style.flexShrink = value
        return self
    }

    public func flex() -> ASLayoutElement {
        self.style.flexShrink = 1.0
        self.style.flexGrow = 1.0
        return self
    }

    public func flexBasis(_ value: ASDimension) -> ASLayoutElement {
        self.style.flexBasis = value
        return self
    }

    public func alignSelf(_ value: ASStackLayoutAlignSelf) -> ASLayoutElement {
        self.style.alignSelf = value
        return self
    }

    public func ascender(_ value: CGFloat) -> ASLayoutElement {
        self.style.ascender = value
        return self
    }

    public func descender(_ value: CGFloat) -> ASLayoutElement {
        self.style.descender = value
        return self
    }
}


//MARK: - ASAbsoluteLayoutElement


extension ASLayoutElement {

    public func layoutPosition(_ value: CGPoint) -> ASLayoutElement {
        self.style.layoutPosition = value
        return self
    }

    public func layoutPosition(_ x: CGFloat, _ y: CGFloat) -> ASLayoutElement {
        self.style.layoutPosition = CGPoint(x: x, y: y)
        return self
    }
}


//MARK: - ASStackLayoutSpec, ASAbsoluteLayoutSpec


extension Array where Element: ASLayoutElement {

    public func stacked(direction: ASStackLayoutDirection = .horizontal,
                        spacing: CGFloat = 0,
                        justify: ASStackLayoutJustifyContent = .start,
                        align: ASStackLayoutAlignItems = .stretch) -> ASStackLayoutSpec {
        return ASStackLayoutSpec(direction: direction,
                                 spacing: spacing,
                                 justifyContent: justify,
                                 alignItems: align,
                                 children: self)
    }

    public func horizontallyStacked(spacing: CGFloat = 0,
                                    justify: ASStackLayoutJustifyContent = .start,
                                    align: ASStackLayoutAlignItems = .stretch) -> ASStackLayoutSpec {
        return ASStackLayoutSpec(direction: .horizontal,
                                 spacing: spacing,
                                 justifyContent: justify,
                                 alignItems: align,
                                 children: self)
    }

    public func verticallyStacked(spacing: CGFloat = 0,
                                  justify: ASStackLayoutJustifyContent = .start,
                                  align: ASStackLayoutAlignItems = .stretch) -> ASStackLayoutSpec {
        return ASStackLayoutSpec(direction: .vertical,
                                 spacing: spacing,
                                 justifyContent: justify,
                                 alignItems: align,
                                 children: self)
    }

    public func absolute(sizing: ASAbsoluteLayoutSpecSizing = .default) -> ASAbsoluteLayoutSpec {
        return ASAbsoluteLayoutSpec(sizing: sizing, children: self)
    }
}


//MARK: - ASDimension helpers


public protocol CGFloatConvertible {

    func cgFloatValue() -> CGFloat
}


public extension CGFloatConvertible {

    public var asPoints: ASDimension {
        return ASDimensionMake(self.cgFloatValue())
    }

    public var asFraction: ASDimension {
        return ASDimensionMakeWithFraction(self.cgFloatValue())
    }
}


extension Double: CGFloatConvertible {
    public func cgFloatValue() -> CGFloat {
        return CGFloat(self)
    }
}


extension Float: CGFloatConvertible {
    public func cgFloatValue() -> CGFloat {
        return CGFloat(self)
    }
}


extension CGFloat: CGFloatConvertible {
    public func cgFloatValue() -> CGFloat {
        return self
    }
}


extension Integer {
    public func cgFloatValue() -> CGFloat {
        return CGFloat(self.toIntMax())
    }
}


extension Int: CGFloatConvertible {
}


extension Int8: CGFloatConvertible {
}


extension Int16: CGFloatConvertible {
}


extension Int32: CGFloatConvertible {
}


extension Int64: CGFloatConvertible {
}


extension UInt: CGFloatConvertible {
}


extension UInt8: CGFloatConvertible {
}


extension UInt16: CGFloatConvertible {
}


extension UInt32: CGFloatConvertible {
}


extension UInt64: CGFloatConvertible {
}
