# ASDKSwiftHelpers

[![CI Status](http://img.shields.io/travis/Mikhail Mulyar/ASDKSwiftHelpers.svg?style=flat)](https://travis-ci.org/Mikhail Mulyar/ASDKSwiftHelpers)
[![Version](https://img.shields.io/cocoapods/v/ASDKSwiftHelpers.svg?style=flat)](http://cocoapods.org/pods/ASDKSwiftHelpers)
[![License](https://img.shields.io/cocoapods/l/ASDKSwiftHelpers.svg?style=flat)](http://cocoapods.org/pods/ASDKSwiftHelpers)
[![Platform](https://img.shields.io/cocoapods/p/ASDKSwiftHelpers.svg?style=flat)](http://cocoapods.org/pods/ASDKSwiftHelpers)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ASDKSwiftHelpers is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ASDKSwiftHelpers"
```

## Author

Mikhail Mulyar, mulyarm@gmail.com

## License

ASDKSwiftHelpers is available under the MIT license. See the LICENSE file for more info.
